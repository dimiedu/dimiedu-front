import '@babel/polyfill'
import 'typeface-spoqa-han-sans2'
import 'boxicons/css/boxicons.min.css'
import 'vue-select/dist/vue-select.css'
import 'sweetalert2/dist/sweetalert2.min.css'

import Vue from 'vue'
import Eduel from './eduel'
import swal from 'vue-sweetalert2'
import Ripple from 'vue-ripple-directive'
import NProgress from 'nprogress'
import VuePerfectScrollbar from 'vue-perfect-scrollbar'
import MonacoEditor from 'monaco-editor-forvue'
import vSelect from 'vue-select'

import App from './App.vue'
import router from './router'

NProgress.configure({ showSpinner: false })
NProgress.start()

Vue.use(Eduel)
Vue.use(swal)

Vue.component('vue-perfect-scrollbar', VuePerfectScrollbar)
Vue.component('monaco-editor', MonacoEditor)
Vue.component('v-select', vSelect)

Ripple.color = 'rgba(255, 255, 255, 0.2)'
Ripple.zIndex = 55
Vue.directive('ripple', Ripple)

new Vue({
  router,
  components: {
    App
  },
  template: '<App />'
}).$mount('#app')

NProgress.done()
