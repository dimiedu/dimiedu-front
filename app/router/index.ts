import Vue from 'vue'
import VueRouter from 'vue-router'
import NProgress from 'nprogress'
import EduWrapper from '@/components/EduWrapper.vue'

import Main from '@/pages/Main.vue'
import ExplorerFound from '@/pages/ExplorerFound.vue'
import MobileFound from '@/pages/MobileFound.vue'
import NotFound from '@/pages/NotFound.vue'

// auth
import Login from '@/pages/paths/auth/Login.vue'

// lecture
import LectureMain from '@/pages/paths/lecture/LectureMain.vue'
import LessonContent from '@/pages/paths/lecture/LessonContent.vue'

// management
import ManageMain from '@/pages/paths/management/ManageMain.vue'
import ManageCurriculum from '@/pages/paths/management/lecture/ManageCurriculum.vue'
import ManageExam from '@/pages/paths/management/lecture/ManageExam.vue'
import ManageExamResult from '@/pages/paths/management/lecture/ManageExamResult.vue'
import ManageProblem from '@/pages/paths/management/lecture/ManageProblem.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: EduWrapper,
    children: [
      {
        path: '/',
        name: 'main',
        components: {
          eduRoute: Main
        }
      },

      {
        path: '/lecture',
        name: 'lecture_main',
        components: {
          eduRoute: LectureMain
        }
      },

      {
        path: '/management',
        name: 'management_main',
        components: {
          eduRoute: ManageMain
        }
      },

      {
        path: '/management/curriculum',
        name: 'management_curriculum',
        components: {
          eduRoute: ManageCurriculum
        }
      },

      {
        path: '/management/exam',
        name: 'management_exam',
        components: {
          eduRoute: ManageExam
        }
      },

      {
        path: '/management/examresult',
        name: 'management_exam_result',
        components: {
          eduRoute: ManageExamResult
        }
      },

      {
        path: '/management/problem',
        name: 'management_problem',
        components: {
          eduRoute: ManageProblem
        }
      },

      {
        path: '/auth/login',
        name: 'login',
        components: {
          eduRoute: Login
        },
        meta: { title: '로그인' }
      }
    ]
  },

  {
    path: '/lecture/lesson',
    name: 'lecture_content',
    component: LessonContent
  },

  {
    path: '/ienope',
    name: 'ienope',
    component: ExplorerFound
  },

  {
    path: '/mobilenope',
    name: 'mobilenope',
    component: MobileFound
  },

  {
    path: '*',
    component: NotFound
  }
]

const router = new VueRouter({
  mode: 'history',
  routes: routes,
  scrollBehavior (to, from, savedPosition): any {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

const isIE = (): any => window.navigator.userAgent.match('MSIE') || window.navigator.userAgent.match('Trident')

const isMobile = (): any => {
  if (navigator.userAgent.match(/iPhone|iPod|Android|Windows CE|BlackBerry|Symbian|Windows Phone|webOS|Opera Mini|Opera Mobi|POLARIS|IEMobile|lgtelecom|nokia|SonyEricsson/i) != null || navigator.userAgent.match(/LG|SAMSUNG|Samsung/) != null) return true
}

router.beforeEach((to, from, next) => {
  NProgress.start()

  if (isIE()) {
    if (to.name === 'ienope') return next()
    else return next({ name: 'ienope' })
  }

  if (isMobile()) {
    if (to.name === 'mobilenope') return next()
    else return next({ name: 'mobilenope' })
  }

  if (to.meta.title) document.title = to.meta.title
  next()
})

router.afterEach(() => {
  NProgress.done()
})

export default router
