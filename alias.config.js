const path = require('path')

const aliases = {
  '@': 'app',
  'styles': 'app/scss'
}

module.exports = {
  webpack: {}
}

Object.keys(aliases).forEach(alias => {
  const aliasTo = aliases[alias]
  module.exports.webpack[alias] = resolveSrc(aliasTo)
})

function resolveSrc (_path) {
  return path.resolve(__dirname, _path)
}
